INTRODUCTION
-------------
This modules outlines every DOM element on your page with a random (valid) CSS hex color.

REQUIREMENTS
------------
There are no special requirements, only recommendation to use it with config split module

INSTALLATION
-------------
Standard installation, enable module and use it. Recommended to put it into config split module

CONFIGURATION
-------------
There is no Config page, by default functionality is turned OFF, you can turn it ON with
ALT + C, you can turn it OFF by doing the same ALT + C